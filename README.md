About omniorb-feedstock
=======================

The official `omniorb` conda packages are built on conda-forge: <https://github.com/conda-forge/omniorb-feedstock>

On conda-forge, `osx-arm64` packages are currently built using cross-compilation.
When cross-compiling omniorb, it's not possible to generate `omniidl` binary, which is required to compile `cpptango`.

This means that with conda-forge omniorb package, we can only cross-compile cpptango for `osx-arm64`. We can't use the package to natively compile on Apple Silicon.

This repository is a forked to compile omniorb natively on `osx-arm64` using GitLab `saas-macos-medium-m1` runners.
The package is published to the [tango-controls](https://anaconda.org/tango-controls/repo) channel under the `omniorb_osx_arm64` label.

Feedstock license: [BSD-3-Clause](https://github.com/conda-forge/omniorb-feedstock/blob/main/LICENSE.txt)

Home: http://omniorb.sourceforge.net/index.html

Package license: GPL-2.0-or-later OR LGPL-2.1-or-later

Summary: Robust high performance CORBA ORB for C++ and Python

Development: https://sourceforge.net/projects/omniorb/

Documentation: http://omniorb.sourceforge.net/docs.html

omniORB is an Object Request Broker (ORB) which implements
specification 2.6 of the Common Object Request Broker Architecture
(CORBA).

Installing omniorb
==================

Installing `omniorb` from the `tango-controls` channel can be achieved by running:


```shell
conda install -c tango-controls/label/omniorb_osx_arm64 omniorb omniorb-libs
```

Note that you need `conda-forge` for the dependencies. If it's not in your default channels, pass it as argument:

```shell
conda install --yes -c tango-controls/label/omniorb_osx_arm64 -c conda-forge omniorb omniorb-libs omniorbpy
```

About conda-forge
=================

[![Powered by
NumFOCUS](https://img.shields.io/badge/powered%20by-NumFOCUS-orange.svg?style=flat&colorA=E1523D&colorB=007D8A)](https://numfocus.org)

conda-forge is a community-led conda channel of installable packages.
In order to provide high-quality builds, the process has been automated into the
conda-forge GitHub organization. The conda-forge organization contains one repository
for each of the installable packages. Such a repository is known as a *feedstock*.

A feedstock is made up of a conda recipe (the instructions on what and how to build
the package) and the necessary configurations for automatic building using freely
available continuous integration services. Thanks to the awesome service provided by
[Azure](https://azure.microsoft.com/en-us/services/devops/), [GitHub](https://github.com/),
[CircleCI](https://circleci.com/), [AppVeyor](https://www.appveyor.com/),
[Drone](https://cloud.drone.io/welcome), and [TravisCI](https://travis-ci.com/)
it is possible to build and upload installable packages to the
[conda-forge](https://anaconda.org/conda-forge) [anaconda.org](https://anaconda.org/)
channel for Linux, Windows and OSX respectively.

To manage the continuous integration and simplify feedstock maintenance
[conda-smithy](https://github.com/conda-forge/conda-smithy) has been developed.
Using the ``conda-forge.yml`` within this repository, it is possible to re-render all of
this feedstock's supporting files (e.g. the CI configuration files) with ``conda smithy rerender``.

For more information please check the [conda-forge documentation](https://conda-forge.org/docs/).

Terminology
===========

**feedstock** - the conda recipe (raw material), supporting scripts and CI configuration.

**conda-smithy** - the tool which helps orchestrate the feedstock.
                   Its primary use is in the construction of the CI ``.yml`` files
                   and simplify the management of *many* feedstocks.

**conda-forge** - the place where the feedstock and smithy live and work to
                  produce the finished article (built conda distributions)
